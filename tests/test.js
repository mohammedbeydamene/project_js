
var test = require('tape');
const {getSomme} = require('./momo');
test('tests function somme', function(t) {
  t.equals(getSomme(1,1), 2);
  t.equals(getSomme(4,3), (3+4));
  t.equals(getSomme(4,6), (6+4));
  t.equals(getSomme(10,22), (11+21));
  t.end();
});
